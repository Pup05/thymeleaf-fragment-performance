package com.fm.fragmentperformance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WidgetController {

  @Autowired
  WidgetService widgetService;

  @RequestMapping(value = "/")
  public String home() {
    return "home";
  }

  @RequestMapping(value = "baseline")
  public String baseline(Model model) {
    model.addAttribute("widgets", widgetService.findAll());
    return "baseline";
  }

  @RequestMapping(value = "replace")
  public String replace(Model model) {
    model.addAttribute("widgets", widgetService.findAll());
    return "replace";
  }

  @RequestMapping(value = "replace-from-common")
  public String replaceFromCommon(Model model) {
    model.addAttribute("widgets", widgetService.findAll());
    return "replace-from-common";
  }

  @RequestMapping(value = "include")
  public String include(Model model) {
    model.addAttribute("widgets", widgetService.findAll());
    return "include";
  }

  @RequestMapping(value = "include-from-common")
  public String includeFromCommon(Model model) {
    model.addAttribute("widgets", widgetService.findAll());
    return "include-from-common";
  }

  @RequestMapping(value = "include-table")
  public String includeTable(Model model) {
    model.addAttribute("widgets", widgetService.findAll());
    return "include-table";
  }
}
