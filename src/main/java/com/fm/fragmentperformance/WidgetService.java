package com.fm.fragmentperformance;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class WidgetService {

  @Value("${numWidgets:100}")
  private int numWidgets;

  private List<Widget> widgets = new ArrayList<>();

  @PostConstruct
  public void init() {

    // generate some widgets
    for (int i = 0; i < numWidgets; i++) {
      Widget widget = new Widget();
      widget.setName(String.format("Widget-%s", i + 1));
      widget.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
      widgets.add(widget);
    }
  }

  public List<Widget> findAll() {
    return widgets;
  }
}
